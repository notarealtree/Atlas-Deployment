# Atlas

Project to serve as information about deployment of the atlas system.

## Commands

- Create network: `docker network create --subnet=172.18.0.0/16 atlas`
- Create Volume: `docker volume create --name mongodb`
- Run DB: `docker run -d --net atlas --ip 172.18.0.152 -v mongodb:/data/db atlas-db`
- Run Proxy: `docker run -d --net atlas --ip 172.18.0.50 -p 80:80 atlas-proxy --name proxy`
- Run GUI: `docker run -d --net atlas --ip 172.18.0.100 atlas-gui --name gui`
- Run API: `docker run -d --net atlas --ip 172.18.0.120 atlas-api --name api`
